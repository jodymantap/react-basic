import './App.css';



function MyInfo() {
  const mine = {
    name: "Muhammad Adnand Jody Pratama",
    blurb: "I am a front-end student of Glints Academy Batch 9. I want to be the new member of Samsan Tech.",
    list : ["Japan", "South Korea", "USA"]
  }
  return (
    <div className="body">
      <header>
        <h1>SamsanTech.com</h1>
      </header>
      <div className="left-sidebar" contenteditable>
        <h3>Members :</h3>
        <ul>
          <li>Nam Do San</li>
          <li>Lee Chul San</li>
          <li>Kim Yong San San</li>
        </ul>
      </div>
      <main contenteditable>
        <h1>Hi, my name is {mine.name}</h1>
        <p>Let me tell you guys something. {mine.blurb}</p>
        <p>Anyway, here's my top 3 destination (country) to visit :</p>
        <ul>
          <li>{mine.list[0]}</li>
          <li>{mine.list[1]}</li>
          <li>{mine.list[2]}</li>
        </ul>
      </main>
      <div class="right-sidebar" contenteditable>Right Sidebar</div>
      <footer contenteditable>Footer Content - SamsanTech.com 2020</footer>
    </div>
  );
}

export default MyInfo;
